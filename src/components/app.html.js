export default (scope) => `
	<style>${scope.css}</style>
	<header>
		<div class="nav">
			<a class="btn" alt="Menu" title="Menu" href="#"></a>
			<div class="hamburger" id="hamburger">
				<div class="bar-1"></div>
				<div class="bar-2"></div>
				<div class="bar-3"></div>
			</div>
			<div class="nav-list" id="menu">
				<ul>
					<li>
						<a alt="Link" title="Link" href="#">Link</a>
					</li>
					<li>
						<a alt="Link" title="Link" href="#">Link</a>
					</li>
					<li>
						<a alt="Link" title="Link" href="#">Link</a>
					</li>
					<li>
						<a alt="Link" title="Link" href="#">Link</a>
					</li>
				</ul>
			</div>
		</div>
		<h1>
			LOGO
		</h1>
		<div class="action-bar">
			<div class="search-button">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M370.068,63.494C329.123,22.549,274.686,0,216.781,0S104.438,22.549,63.494,63.494C22.549,104.438,0,158.876,0,216.78c0,57.905,22.549,112.343,63.494,153.287c40.944,40.944,95.383,63.494,153.287,63.494s112.342-22.55,153.287-63.494c40.944-40.944,63.494-95.382,63.494-153.287C433.561,158.876,411.012,104.438,370.068,63.494z M216.78,392.196c-96.725,0-175.416-78.691-175.416-175.416S120.056,41.364,216.781,41.364s175.415,78.691,175.415,175.416S313.505,392.196,216.78,392.196z"/></g></g><g><g><path d="M505.943,476.693L369.981,340.732c-8.077-8.077-21.172-8.077-29.249,0c-8.076,8.077-8.076,21.172,0,29.249l135.961,135.961c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.587-2.019,14.625-6.058C514.019,497.865,514.019,484.77,505.943,476.693z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
			</div>
			<div class="open-task-wrapper-button" id="open-task-wrapper-button">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M256,0c-11.422,0-20.682,9.26-20.682,20.682v470.636c0,11.423,9.26,20.682,20.682,20.682c11.423,0,20.682-9.259,20.682-20.682V20.682C276.682,9.26,267.423,0,256,0z"/></g></g><g><g><path d="M491.318,235.318H20.682C9.26,235.318,0,244.578,0,256c0,11.423,9.26,20.682,20.682,20.682h470.636c11.423,0,20.682-9.259,20.682-20.682C512,244.578,502.741,235.318,491.318,235.318z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
			</div>
		</div>
	</header>
	<main class="wrapper">
		<todo-form></todo-form>
		<over-view></over-view>
		<todo-list ref="list"></<todo-list>
	</main>
	<footer>
		<div class="credits">Icons made by <a href="https://www.flaticon.com/authors/cole-bemis" title="Cole Bemis">Cole Bemis</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
	</footer>
`;
