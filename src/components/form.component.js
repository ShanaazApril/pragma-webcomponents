import template from './form.html';
import css from './form.css';

class Form extends HTMLElement {
  connectedCallback() {
    this.innerHTML = template({css});
  //this.innerHTML = `<h2>Form h2</h2>`;

  }
  render() {    
    this.refs = {
      text: this.querySelector('[ref="text"]'),
      form: this.querySelector('[ref="form"]')
    }
    this.refs.form.addEventListener('submit', (e) => {
      this.props.onSubmit({
        text: this.refs.text.value
      });
      this.refs.form.reset();
    });
  }
}

customElements.define('todo-form', Form);
