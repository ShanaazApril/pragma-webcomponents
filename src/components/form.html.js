export default (scope) => `
<style>${scope.css}</style>
<div id="new-task-wrapper">
	<form ref="form">
		<div class="task-wrapper">
			<input id="new-task" type="text" placeholder="Task Name" ref="text">
		</div>
		<button class="add-task-button" id="add-task-button" ref="btn-submit">
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><g><path d="M256,0c-11.422,0-20.682,9.26-20.682,20.682v470.636c0,11.423,9.26,20.682,20.682,20.682c11.423,0,20.682-9.259,20.682-20.682V20.682C276.682,9.26,267.423,0,256,0z"/></g></g><g><g><path d="M491.318,235.318H20.682C9.26,235.318,0,244.578,0,256c0,11.423,9.26,20.682,20.682,20.682h470.636c11.423,0,20.682-9.259,20.682-20.682C512,244.578,502.741,235.318,491.318,235.318z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
		</button>
    </form>
</div>
`;
