import template from './overview.html';
import css from './overview.css';

class Overview extends HTMLElement {
  connectedCallback() {
    this.innerHTML = template({css});
  }
}

customElements.define('over-view', Overview);
