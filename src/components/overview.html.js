export default (scope) => `
<style>${scope.css}</style>
<h2>Overview:</h2>
<ul class="overview">
	<li class="to-do">
		<span>To do:</span>
		<div class="info" id="perc-to-do">100%</div>			
	</li>
	<li class="completed">
		<span>Completed:</span>
		<div class="info" id="perc-completed">0%</div>
	</li>
</ul>`;

