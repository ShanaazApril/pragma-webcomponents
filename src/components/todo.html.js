export default (scope) => `
<li class="card ${scope.done ? 'done' : ''}">
	<span>
		${scope.text ? `<h3>${scope.text}</h3>` : ''}
	</span>
	<div class="action-bar">
		${(scope.actions || []).map((act) => {
			const icon = act.icon || act;
			const btnNameExt = act.classNames || act;
			const classes =	 ([]).concat(`btn-${btnNameExt}`).join(' ');
				return `<div data-id="${scope.id}" class="${classes}">${icon}</div>`;
		}).join(' ')}
	</div>
</li>`;
