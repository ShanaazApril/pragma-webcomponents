import './todo.component';
import template from './todos.html';
import css from './todos.css';

class List extends HTMLElement {
  connectedCallback() {
    this.innerHTML = template({css});
    this.list = this.querySelector('[ref="todos"]');
    this.list.addEventListener('click', (e) => {
      console.log(e);
      if(e.target.classList.contains('.btn-done')) {
        this.props.actions.done(e.target.dataset.id)
        this.querySelector(`[ref="${e.target.dataset.id}"]`).markComplete();
      }
      if(e.target.classList.contains('.btn-not-done')) {
        this.props.actions.done(e.target.dataset.id)
        this.querySelector(`[ref="${e.target.dataset.id}"]`).markIncomplete();
      }
      if(e.target.classList.contains('.btn-remove')) {
        this.props.actions.remove(e.target.dataset.id)
        this.querySelector(`[ref="${e.target.dataset.id}"]`).remove();
      }
    })
  }

  createAndAppend(task) {
    if (this.querySelector(`[ref=${task.id}]`)) {
      return;
    }

    const taskEl = document.createElement('todo-task');
    taskEl.setAttribute('ref', task.id);
    taskEl.setAttribute('id', task.id);
    this.list.appendChild(taskEl);
    taskEl.render(task);
  }
  render(tasks = this.props.tasks || {}) {
    Object.keys(tasks).forEach(key => {
      const task = tasks[key]
      this.createAndAppend(task);
    });
  }
}

customElements.define('todo-list', List);
