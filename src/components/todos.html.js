export default (scope) => `
<style>${scope.css}</style>
<h2>To do items:</h2>
<ul ref="todos" class="todo"></ul>`;